fetch(`./annunci.json`).then(response => response.json())
.then(data =>{
    // Function
    function populateAds(data){
        const adsWrap = document.querySelector('#annunci-wrap')

        adsWrap.innerHTML = '';

        data.forEach(ad => {
            let div = document.createElement('div')
            div.classList.add('col-12', 'col-md-6', 'col-lg-4', 'my-3')
            div.innerHTML = `<div class=" cat-card  text-white py-3 d-flex justify-content-between flex-column">
            <img class="img-fluid" src="https://picsum.photos/1080/720" alt="">
            <div class="d-flex py- px-2 justify-content-between">
            <span class='tool position-relative mt-2'>
            <span class='toolhover'>${ad.name}</span>
            <p class="fs-4">${truncate(ad.name)}
            </p>
            </span>
            <p class="fs-4">$${ad.price}</p>
          </div>
          <span class='mb-3 ms-2'>${ad.category}</span>
            <button class="btn btn-white ms-1">Esplora<i class="far fa-hand-point-right ps-1"></i></button>
          </div>`

          adsWrap.appendChild(div)
        })
    }

    function truncate(str){
        let truncated = str.split (' ')
        if(truncated.length===1){
        return truncated[0]
        } else {
            return truncated[0] + '...'
        }

    }

    

    function populateCatFilter(){
      const catWrap = document.querySelector('#category-wrapper')
      let categories = new Set(data.map(ad => ad.category));
      categories.forEach(ad =>{
        let div = document.createElement('div')
        div.classList.add('form-check')
        div.innerHTML = `
                          <input class="form-check-input category-filter" type="checkbox" value="${ad}" id="category-${ad}">
                          <label class="form-check-label" for="category-${ad}">
                             ${ad}
                          </label>
                          `
        catWrap.appendChild(div)
      })
    }

    function filterCat(){
      let input = document.querySelectorAll('.category-filter')
      input.forEach(el =>{
        el.addEventListener('click', function(){
          let checked = Array.from(input).filter(el => el.checked).map(el => el.value)
          
          if(checked.length === 0){
            populateAds(data)
          }else{
          globalFiltered = globalFiltered.filter(ad => checked.includes(ad.category))
          populateAds(globalFiltered)
          console.log(globalFiltered);
          }
        })
      })
    }

    function priceOrder(){
      const orderInput = document.querySelectorAll('.price-input')
      orderInput.forEach(input =>{
        input.addEventListener('click', function(){
          let selected = Array.from(orderInput).find(el => el.checked).value
          switch (selected){
            case '1' :
            globalFiltered.sort((a , b) => Number(a.price) - Number(b.price)) ;
            populateAds(globalFiltered)
            break;

            case '2' :
              globalFiltered.sort((a , b) => Number(b.price) - Number(a.price)) ;
              populateAds(globalFiltered)
            break;

            default:
              break;
          }
        })

      })
      
    }




    let globalFiltered = Array.from(data) 
    
    


    populateCatFilter()
    populateAds(data)
    filterCat()
    priceOrder()






})


{/* <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                              Default checkbox
                            </label>
                          </div> */}