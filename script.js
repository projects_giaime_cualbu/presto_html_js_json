
document.addEventListener('scroll', function(e){
    
    
    const navbar = document.querySelector('#p-navbar')
    
    if (window.scrollY > 150){
        navbar.classList.add('active')
        
    }else{
        navbar.classList.remove('active')
    }
})

const rotate = document.querySelector('#rotate-icon')
rotate.addEventListener('click', function(){
    rotate.classList.toggle ('fa-rotate-90')
})
let categories = ['Auto' , 'Moto' , 'Case' , 'Vestiti' , 'Elettronica' , 'Gaming']

const cardWrap = document.querySelector('#card-wrapper')

categories.forEach (category =>{
    let card = document.createElement('div')
    card.classList.add('col-12', 'col-md-4', 'cat-card', 'text-center', 'text-white', 'py-3', 'mx-3', 'my-3')
    card.innerHTML = `<i class="fas fa-store fa-2x"></i>
    <h3>${category}</h3>
    <button class="btn btn-white">Vai alla Categoria</button>`
    
    cardWrap.appendChild(card)
    
    
})

new Glide('.glide', {
    type :'slider',
    startAt: 0,
    perView: 3,
    gap: 20,
    autoplay: 3000,
    animationDuration: 1000,
    breakpoints: {
        1240: {
            perView: 2
        },
        600: {
            perView: 1
        }
    }
    
}).mount()